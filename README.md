
# README

Code to reproduce figures and experiments in "OPTIMAL QUANTIZATION OF RANK-ONE MATRICES IN FLOATING-POINT ARITHMETIC—WITH APPLICATIONS TO BUTTERFLY FACTORIZATIONS" (R. Gribonval, T. Mary, E. Riccietti)

In the interest of reproducible research, we provide Matlab implementations of the algorithms from the above paper, and scripts to reproduce the figures.

Some of the figures involve intermediate computations that may take some time to run, so we also provide saved results to simply display the figures.
 

## Algorithms:

**algo5_1_opt_lambda.m**: given vectors x,y and an integer t, computes the optimal quantization of (xy)^T with t bits (readable version for small matrices)

**algo5_1_opt_lambda_butterfly.m**: given vectors x,y and an integer t, computes the optimal quantization of (xy)^T with t bits (optimized version for cpu time for larger butterfly matrices)

**algo7_2_pairwise.m**: given B1,..,BL butterfly factors and an integer t, computes the optimal quantization of the product B1..BL with t bits by the pairwise heuristic (incorporates Algorithm 7.1 + Algorithm 7.2)

**algo7_3_left2right.m**: given B1,..,BL butterfly factors and an integer t, computes the optimal quantization of the product B1..BL with t bits by the left-to-right heuristic (incorporates Algorithm 7.1 + Algorithm 7.3)


  

  

## Additional functions:

**chop.m**: quantizes matrices to t bits

**Ftsets.m**: produces machine numbers in Ft, FtFt, F2t

** generate_results_butterfly_left2right.m and generate_results_butterfly_pairwise.m: produce results of the heuristic strategies on butterfly factors (butterfly_nfactorsvaries_t%d_bottomup.mat, butterfly_nfactorsvaries_t%d_left2right.mat)


  

  

  

## Files for reproducing figures:

**Fig3_1_machine_elements.m**: generates Fsets.mat

**Fig3_2_average_relative_errors.m** (requires saved_results/Ftsets.mat)

**Fig3_2_worst_relative_error.m** (requires saved_results/Ftsets.mat)

**Fig3_3_relative_errors.m** (requires saved_results/Ftsets.mat)

**Fig4_1_breakpoints.m**

**Figures_sec6.m** (requires saved_results/rank1_opt_n%d.mat, saved_results/rank1_opt_t%d)

**Fig7_2_7_3_butterfly.m** (requires saved_results/butterfly_nfactorsvaries_t%d_bottomup.mat, saved_results/butterfly_nfactorsvaries_t%d_left2right.mat)

  

  

## Saved results:

**Ftsets.mat**: contains Ft, F2t,FtFt

**rank1_opt_n%d.mat, rank1_opt_t%d**: contain data for optimal algorithm and RTN for various n,t, generated running algo5_1_opt.m with different n and t

**butterfly_nfactorsvaries_t%d_bottomup.mat, butterfly_nfactorsvaries_t%d_left2right.mat**: contain data for heuristic strategies on butterfly factors, produced by generate_results_butterfly_left2right.m and generate_results_butterfly_pairwise.m
