%% Generates Fig. 3.1

clear all; close all;
nbits = 3;
emax  = 2;

Ft = gen_float(nbits, emax);
F2t = gen_float(2*nbits, emax);
FtFt = gen_set_of_FP_products(Ft);
Ftp1 = gen_float(nbits+1, emax);
F2tm1 = gen_float(2*nbits-1, emax);

Ft = Ft(Ft>=1 & Ft<=2);
FtFt = FtFt(FtFt>=1 & FtFt<=2);
F2t = F2t(F2t>=1 & F2t<=2);
Ftp1 = Ftp1(Ftp1>=1 & Ftp1<=2);
F2tm1 = F2tm1(F2tm1>=1 & F2tm1<=2);

lFt = length(Ft);
lFtFt = length(FtFt);
lF2t = length(F2t);
lFtp1 = length(Ftp1);
lF2tm1 = length(F2tm1);

machepsFt   = max(abs(Ft(1:end-1)-Ft(2:end))./Ft(1:end-1))/2;
machepsFtFt = max(abs(FtFt(1:end-1)-FtFt(2:end))./FtFt(1:end-1))/2;
machepsF2t  = max(abs(F2t(1:end-1)-F2t(2:end))./F2t(1:end-1))/2;
machepsFtp1 = max(abs(Ftp1(1:end-1)-Ftp1(2:end))./Ftp1(1:end-1))/2;
machepsF2tm1= max(abs(F2tm1(1:end-1)-F2tm1(2:end))./F2tm1(1:end-1))/2;


fprintf('\n');
fprintf('Number of elements in [1,2]:\n');
fprintf('  Ft:    %d\n',lFt);
fprintf('  Ftp1:  %d\n',lFtp1);
fprintf('  FtFt:  %d\n',lFtFt);
fprintf('  F2tm1: %d\n',lF2tm1);
fprintf('  F2t:   %d\n',lF2t);
fprintf('Worst case relative error of:\n');
fprintf('  Ft:    %.1e\n',machepsFt);
fprintf('  Ftp1:  %.1e\n',machepsFtp1);
fprintf('  FtFt:  %.1e\n',machepsFtFt);
fprintf('  F2tm1: %.1e\n',machepsF2tm1);
fprintf('  F2t:   %.1e\n',machepsF2t);

figure()
ms = 7;
plot(Ft,1.1*ones(lFt,1),'-o','markersize',ms);
hold on;
plot(Ftp1,1.05*ones(lFtp1,1),'-v','markersize',ms);
plot(FtFt,1*ones(lFtFt,1),'-*','markersize',ms);
plot(F2tm1,0.95*ones(lF2tm1,1),'-^','markersize',ms);
plot(F2t,0.9*ones(lF2t,1),'-s','markersize',ms);
legend('$$F_t$$','$$F_{t+1}$$','$$F_tF_t$$','$$F_{2t-1}$$','$$F_{2t}$$','location','north','orientation','horizontal','interpreter','latex','fontsize',14);
ylim([0.8 1.2]);
yticks([]);
set(gcf,'color','w');
export_fig('positions.pdf');

save('saved_results/Fsets')


return

function F = gen_float(t, emax, include_subn)
% generates a floating point number system with t bits of precision and
% (1-emax, emax) exponent range
  % include subnormals by default
  if nargin<3
    include_subn = 0;
  end
  if include_subn
    m = 1:(2^t-1);
  else
    m = (2^(t-1)):(2^t-1);
  end
  emin = 1-emax;
  F = [];
  for e=emin:emax
    F=[F m*2^(e-t)];
  end
  F=[F 2^emax];
end

function S = gen_set_of_FP_products(F)
% generate set of xy where x,y are in F
  S = [];
  for i=1:length(F)
    for j=1:length(F)
      S = [S F(i)*F(j)];
      truc=F(i)*F(j);
      if truc==1.09375
        fprintf('%f %f\n',F(i),F(j))
      end
    end
  end
  % remove duplicates
  S = unique(S);
end
