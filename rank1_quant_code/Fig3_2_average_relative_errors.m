%% Generates Fig. 3.2 right, average relative errors on Ft, FtFt, F2t

clear all; close all;
emax  = 2;

load('datasets/Ftsets.mat');

z=linspace(1,2,1000);


i=0;
for t=2:17
  i=i+1;
  F = Ft{t};
  eta = 0;
  for j=1:length(F)-1
    mj = (F(j)+F(j+1))/2;
    eta = eta + F(j)*log(F(j)) + F(j+1)*log(F(j+1)) - 2*mj*log(mj);
  end
  etaFt(i) = eta;
  if t<=11
    eta = 0;
    F = FtFt{t};
    for j=1:length(F)-1
      mj = (F(j)+F(j+1))/2;
      eta = eta + F(j)*log(F(j)) + F(j+1)*log(F(j+1)) - 2*mj*log(mj);
    end
    etaFtFt(i) = eta;

    eta = 0;
    F = F2t{t};
    for j=1:length(F)-1
      mj = (F(j)+F(j+1))/2;
      eta = eta + F(j)*log(F(j)) + F(j+1)*log(F(j+1)) - 2*mj*log(mj);
    end
    etaF2t(i) = eta;
  end
end

xx=(4:11)';
 y=log2(etaFtFt(3:end))';
 X = [ones(length(xx),1) xx];
 b = X\y;
yCalc = X*b;

yCalc=2.^(yCalc);
  
ms=10;
lw=1.5;
semilogy(4:17,etaFt(3:end),'-o','markersize',ms,'linewidth',lw);
hold on;
semilogy(4:11,etaFtFt(3:end),'-*','markersize',ms,'linewidth',lw);
semilogy(4:11,etaF2t(3:end),'-s','markersize',ms,'linewidth',lw);
semilogy(4:17,2^4*etaFtFt(3)*2.^(-(4:17)),'--k','markersize',ms,'linewidth',lw);
semilogy(4:11,2^8*etaFtFt(3)*2.^(-2*(4:11)),'-.k','markersize',ms,'linewidth',lw);
semilogy(4:11,yCalc,':k','linewidth',lw);
legend('$$\eta(F_t)$$','$$\eta(F_tF_t)$$','$$\eta(F_{2t})$$','$$O(2^{-t})$$','$$O(2^{-2t})$$',sprintf('fit: $$O(2^{%.1ft})$$',b(2)),'interpreter','latex','fontsize',14);
xlabel('$$t$$','interpreter','latex','fontsize',14);
ylim([1e-8 1e-1]);
set(gcf,'color','w');
%export_fig('eta.pdf');


return

