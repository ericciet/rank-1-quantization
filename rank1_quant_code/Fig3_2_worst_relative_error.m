%%%% Generates Fig. 3.2 left, worst case relative errors
clear all; close all;
emax  = 2;

% Computes worst case relative errors for Ft, FtFt, F2t
load('saved_results/Ftsets.mat');
i=0;
for t=2:17
  i=i+1;
  F = Ft{t};
  machepsFt(i) = max(abs(F(1:end-1)-F(2:end))./(F(1:end-1)+F(2:end)));
  if t<=11
    F = FtFt{t};
    machepsFtFt(i) = max(abs(F(1:end-1)-F(2:end))./(F(1:end-1)+F(2:end)));
    F = F2t{t};
    machepsF2t(i) = max(abs(F(1:end-1)-F(2:end))./(F(1:end-1)+F(2:end)));
  end
end

% fit
 xx=(4:11)';
 y=log2(machepsFtFt(3:end))';
 X = [ones(length(xx),1) xx];
 b = X\y;
yCalc = X*b;

yCalc=2.^(yCalc);
  
ms=10;
lw=1.5;
semilogy(4:17,machepsFt(3:end),'-o','markersize',ms,'linewidth',lw);
hold on;
semilogy(4:11,machepsFtFt(3:end),'-*','markersize',ms,'linewidth',lw);
semilogy(4:11,machepsF2t(3:end),'-s','markersize',ms,'linewidth',lw);
semilogy(4:17,2^4*machepsFtFt(3)*2.^(-(4:17)),'--k','markersize',ms,'linewidth',lw);
semilogy(4:11,2^8*machepsFtFt(3)*2.^(-2*(4:11)),'-.k','markersize',ms,'linewidth',lw);
semilogy(4:11,yCalc,':k','linewidth',lw);
legend('$$\epsilon(F_t)$$','$$\epsilon(F_tF_t)$$','$$\epsilon(F_{2t})$$','$$O(2^{-t})$$','$$O(2^{-2t})$$',sprintf('fit: $$O(2^{%.1ft})$$',b(2)),'interpreter','latex','fontsize',14);
xlabel('$$t$$','interpreter','latex','fontsize',14);
ylim([1e-8 1e-1]);
set(gcf,'color','w');
export_fig('epsilon.pdf');



return

