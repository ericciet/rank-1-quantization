%% Generates Fig. 3.3

clear all; close all;
nbits = 4;
emax  = 2;

Ft = gen_float(nbits, emax);
F2t = gen_float(2*nbits, emax);
FtFt = gen_set_of_FP_products(Ft);
Ft = Ft(Ft>=1 & Ft<=2);
F2t = F2t(F2t>=1 & F2t<=2);
FtFt = FtFt(FtFt>=1 & FtFt<=2);

z=linspace(1,2,1000);
for i=1:length(z)
  errFt(i) = min(abs(Ft-z(i))/z(i));
  errFtFt(i) = min(abs(FtFt-z(i))/z(i));
  errF2t(i) = min(abs(F2t-z(i))/z(i));
end

load('saved_results/Ftsets.mat');

eta = 0;
F = FtFt{nbits};
for j=1:length(F)-1
  mj = (F(j)+F(j+1))/2;
  eta = eta + F(j)*log(F(j)) + F(j+1)*log(F(j+1)) - 2*mj*log(mj);
end



figure()
lw=2;
semilogy(z,errFt,'-','linewidth',lw);
hold on;
semilogy(z,errFtFt,'-','linewidth',lw);
ylim([0 2^-nbits]);
semilogy(z,errF2t,'-','linewidth',lw);
semilogy(z,eta*ones(length(z)),'--k','linewidth',lw);
hleg=legend('$$d(z,F_t)/|z|$$','$$d(z,F_tF_t)/|z|$$','$$d(z,F_{2t})/|z|$$','$$\eta(F_tF_t)$$');
set(hleg,'location','south','interpreter','latex','fontsize',14);
xlabel('$$z$$','interpreter','latex','fontsize',14);
set(gcf,'color','w');
export_fig(sprintf('average_t%d.pdf',nbits));

return

function F = gen_float(t, emax, include_subn)
% generates a floating point number system with t bits of precision and
% (1-emax, emax) exponent range
  % include subnormals by default
  if nargin<3
    include_subn = 0;
  end
  if include_subn
    m = 1:(2^t-1);
  else
    m = (2^(t-1)):(2^t-1);
  end
  emin = 1-emax;
  F = [];
  for e=emin:emax
    F=[F m*2^(e-t)];
  end
  F=[F 2^emax];
end

function S = gen_set_of_FP_products(F)
% generate set of xy where x,y are in F
  S = [];
  for i=1:length(F)
    for j=1:length(F)
      S = [S F(i)*F(j)];
    end
  end
  % remove duplicates
  S = unique(S);
end
