%%%%%%%% Generates Fig. 4.1

clear all; close all;
rng(1)
d = 5;
lspace=logspace(-2,2,d)';
x= rand(d,1);%.*lspace(randperm(d));
y= rand(d,1);%.*lspace(randperm(d));
t = 4;



opterr = inf;

nrmx2 = norm(x)^2;
nrmy2 = norm(y)^2;

%build_breakpoints
x12 = x./(2.^(floor(log2(x))));
l1 = [1];
for i=1:d
  for e=1:2
    for m=(2^(t-1)):(2^t-1)
      nextl = (m+0.5)*2^(e-t)/x12(i);
      if nextl < 2 && nextl > 1
        l1 = [l1 nextl];
      end
    end
  end
end


% sort_breakpoints
l1 = sort(l1);


% search_opt
L1=length(l1);
l1mid = zeros(L1-1,1);
l1mid = (l1(1:end-1)+l1(2:end))/2;
for i=1:L1-1
  xh = chop(l1mid(i)*x,t);
  xtxh = x'*xh;
  nrmxh2 = norm(xh)^2;
  l2 = xtxh/nrmxh2;
  yh = chop(l2*y,t);
  nrmyh2 = norm(yh)^2;
  err(i) = sqrt(nrmx2*nrmy2 + nrmxh2*nrmyh2 - 2*xtxh*(y'*yh));
  if i==6 || i==7
  end
  if err(i) < opterr
    opterr = err(i);
    l1opt = l1mid(i);
  end
end
 

ntest=1e4;
errtest=zeros(ntest,1);
i = 0;
for ltest=linspace(1,2,ntest)
  i = i+1;
  xh = chop(ltest*x,t);
  l2test(i) = x'*xh/norm(xh)^2;
  yh = chop(l2test(i)*y,t);
  errtest(i) = norm(x*y'-xh*yh','fro');
end


figure(1)
plot(linspace(1,2,ntest),errtest,'-');
hold on;
plot(l1mid,err,'*','markersize',7);
xlim([1 2]);
xlabel('$\lambda$','Interpreter','latex')
legend('$$C_{x,y}(\mathrm{round}(\lambda x),\mathrm{round}(\mu(\widehat{x})y))$$','$$\lambda_{j+1/2}$$ (midpoints)','interpreter','latex','fontsize',14);
ylim([0 0.05]);
set(gcf,'color','w');
%export_fig('breakpoints.pdf');


figure(2)
plot(linspace(1,2,ntest),linspace(1,2,ntest).*l2test,'-')
ylim([0,2])
xlabel('$\lambda$','interpreter','latex')
legend('$$\lambda\mu(\widehat{x})$$','interpreter','latex','fontsize',14)
set(gcf,'color','w');
%export_fig('lambda_mu.pdf');


