%% Generates Fig. 4.1 right 
nsamples=1;
n=16;
t=11;

nlambda=1000;
lambda=linspace(1,2,nlambda);


lopt=zeros(nsamples,1);
opt_err=zeros(nsamples,1);
scaled_err=zeros(nsamples,nlambda);
scaled_err_mu=zeros(nsamples,nlambda);
rtn_err=zeros(nsamples,1);

ratio_rtn=zeros(nsamples,1);
ratio_scaled=zeros(nsamples,nlambda);
ratio_scaled_mu=zeros(nsamples,nlambda);






for i=1:nsamples
    %% data
    lspace=logspace(-2,2,n)';
    x= rand(n,1).*lspace(randperm(n));
    y= rand(n,1).*lspace(randperm(n));
    xy=x*y';
    nxy=norm(xy,'fro');
    
    
    %% optimal algorithm
    [lopt(i),~,~,~,opt_err(i),rtn_err(i),ratio_rtn(i)]=algo5_1_opt_lambda(x,y,t,1);
    
   
    opterr=opt_err(i);
    for j=1:nlambda
        scaled_err(i,j) = norm(xy-chop(lambda(j)*x,t)*chop(1/lambda(j)*y,t)', 'fro')/nxy;
        ratio_scaled(i,j)=scaled_err(i,j)/opterr;
    end

    for j=1:nlambda
        xh = chop(lambda(j)*x,t);
        xtxh = x'*xh;
        nrmxh2 = norm(xh)^2;
        mu = xtxh/nrmxh2;
        scaled_err_mu(i,j) = norm(xy-xh*chop(mu*y,t)', 'fro')/nxy;
        ratio_scaled_mu(i,j)=scaled_err_mu(i,j)/opterr;
    end

   

end
close all
figure(5)


plot(lambda,ratio_scaled)
hold on
plot(lambda,ratio_scaled_mu)
plot(lopt,1,'*k',MarkerSize=10,MarkerFaceColor='k')
plot(1,ratio_rtn,'ok',MarkerSize=10,MarkerFaceColor='k')
plot(lambda,ones(length(lambda),1),':k')

legend('$\varphi(\lambda)=1/\lambda$','$\varphi(\lambda)=\mu(\lambda)$','OPT ($\lambda^*,\mu^*$)','RTN ($\lambda=\varphi(\lambda)=1$)','interpreter','latex')
xlabel('$\lambda$','interpreter','latex')
ylabel('$\rho_{(\lambda,\varphi(\lambda))}/\rho_{\mathtt{OPT}}$','interpreter','latex')
set(gcf,'color','w');
exportgraphics(gcf,'fig_mu_star.pdf');



