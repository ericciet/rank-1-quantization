%% Generates Figures 7.2, 7.3
% Uses saved results produced by generate_results_butterfly_left2right.m
% and generate_results_butterfly_pairwise.m
% Comparison of RTN, left to right and pairwise heuristics for butterfly factors 

clear all; 
close all;

% fontsize
fs = 14;
% markersize
ms = 8;
% linewidth
lw = 1.5;

set(groot,'defaultLineMarkerSize',ms);
set(groot,'defaultLineLineWidth',lw);
set(groot,'defaultAxesFontSize',fs);
set(groot,'defaultAxesTickLabelInterpreter','latex'); 
set(groot,'defaultTextInterpreter','latex'); 
set(groot,'defaultLegendInterpreter','latex');
set(groot,'defaultfigurecolor','w')

t = 8;
pp = 4:2:18;
nn = 2.^pp;

load(sprintf('saved_results/butterfly_nfactorsvaries_t%d_pairwise.mat',t));
time_BU = time_opt;
err_rtn_BU = err_rtn;
err_opt_BU = err_opt;
load(sprintf('saved_results/butterfly_nfactorsvaries_BIGGER_t%d_pairwise.mat',t));
time_BU = [time_BU; time_opt];
err_rtn_BU = [err_rtn_BU; err_rtn];
err_opt_BU = [err_opt_BU; err_opt];
load(sprintf('saved_results/butterfly_nfactorsvaries_t%d_left2right.mat',t));
time_L2R = time_opt;
err_opt_L2R = err_opt;
load(sprintf('saved_results/butterfly_nfactorsvaries_BIGGER_t%d_left2right.mat',t));
time_L2R = [time_L2R; time_opt];
err_opt_L2R = [err_opt_L2R; err_opt];



figure(1)
loglog(nn,err_opt_BU,'-*');
hold on;

loglog(nn,err_opt_L2R,'-s');
loglog(nn,err_rtn_BU,'-o');
xlabel('$$n$$');
ylabel('Error');
legend('Pairwise','Left-to-right','RTN','location','northwest','interpreter','latex')
exportgraphics(gcf,'fig_butterfly_BUvsL2R_pvaries_t8_error.pdf')



% fit
xx=log(nn(2:end));
y=log(time_BU(2:end));
X = [ones(length(xx),1) xx'];
bBU = X\y;
yBU = X*bBU;
yBU=exp(yBU);

y=log(time_L2R(2:end));
bL2R = X\y;
yL2R = X*bL2R;
yL2R=exp(yL2R);

figure(2)
loglog(nn,time_BU,'-*');
hold on;
loglog(nn,time_L2R,'-s');
loglog(nn(2:end),yBU,'-.k',nn(2:end),yL2R,'--k')
xlabel('$$n$$');
ylabel('Time');
legend(sprintf('Pairwise (fit: $$O(n^{%.1f})$$)',bBU(2)),...
    sprintf('Left-to-right (fit: $$O(n^{%.1f})$$)',bL2R(2)),...
    'location','northwest','interpreter','latex')
exportgraphics(gcf,'fig_butterfly_BUvsL2R_pvaries_t8_time.pdf')




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

p = 16;
tt = 2:11;

load(sprintf('saved_results/butterfly_%dfactors_tvaries_pairwise.mat',p));
time_BU = time_opt;
err_rtn_BU = err_rtn;
err_opt_BU = err_opt;
load(sprintf('saved_results/butterfly_%dfactors_tvaries_left2right.mat',p));
time_L2R = time_opt;
err_rtn_L2R = err_rtn;
err_opt_L2R = err_opt;

% fit
xx=tt';
X = [ones(length(xx),1) xx];

y=log2(err_rtn_BU)';
brtn = X\y;
yrtn = X*brtn;
yrtn=2.^(yrtn);

y=log2(err_opt_BU)';
bBU = X\y;
yBU = X*bBU;
yBU=2.^(yBU);

y=log2(err_opt_L2R)';
bL2R = X\y;
yL2R = X*bL2R;
yL2R=2.^(yL2R);

figure(3)
semilogy(tt,err_opt_BU,'-*');
hold on;
semilogy(tt,err_opt_L2R,'-s');
semilogy(tt,err_rtn_BU,'-o');
semilogy(tt,yrtn,':k',tt,yBU,'-.k',tt,yL2R,'--k');
xlabel('$$t$$');
ylabel('Error');
legend(sprintf('Pairwise (fit: $$O(2^{%.1ft})$$)',bBU(2)),...
       sprintf('Left-to-right (fit: $$O(2^{%.1ft})$$)',bL2R(2)),...
       sprintf('RTN (fit: $$O(2^{%.1ft})$$)',brtn(2)))
exportgraphics(gcf,'fig_butterfly_BUvsL2R_p16_tvaries_error.pdf')


figure(4)
plot(tt,time_BU,'-*');
hold on;
plot(tt,time_L2R,'-s');
xlabel('$$t$$');
ylabel('Time');
legend('Pairwise','Left-to-right ');
exportgraphics(gcf,'fig_butterfly_BUvsL2R_p16_tvaries_time.pdf')

