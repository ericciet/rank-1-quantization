%% Generates figures in section 6   
close all;
clear all;

% fontsize
fs = 14;
% markersize
ms = 8;
% linewidth
lw = 1.5;

set(groot,'defaultLineMarkerSize',ms);
set(groot,'defaultLineLineWidth',lw);
set(groot,'defaultAxesFontSize',fs);
set(groot,'defaultAxesTickLabelInterpreter','latex'); 
set(groot,'defaultTextInterpreter','latex'); 
set(groot,'defaultLegendInterpreter','latex');
set(groot,'defaultfigurecolor','w')

opt_err_t=[];
rtn_err_t=[];
pso_err_t=[];
opt_err_n=[];
rtn_err_n=[];
pso_err_n=[];
opt_time_t=[];
pso_time_t=[];
opt_time_n=[];
pso_time_n=[];
nn=[16,128,1024];
tt=[4,8,11];

load('saved_results/Ftsets.mat');
i=0;
for t=2:11
  i=i+1;
  F = Ft{t};
  machepsFt(i) = max(abs(F(1:end-1)-F(2:end))./(F(1:end-1)+F(2:end)));
  F = FtFt{t};
  machepsFtFt(i) = max(abs(F(1:end-1)-F(2:end))./(F(1:end-1)+F(2:end)));
  F = F2t{t};
  machepsF2t(i) = max(abs(F(1:end-1)-F(2:end))./(F(1:end-1)+F(2:end)));
end

for t=2:11
load(sprintf('saved_results/rank1_opt_t%d',t))
opt_err_t=[opt_err_t;max(opt_err)];
rtn_err_t=[rtn_err_t;max(rtn_err)];
pso_err_t=[pso_err_t;max(pso_err)];
opt_time_t=[opt_time_t;mean(time_optimal)];
pso_time_t=[pso_time_t;mean(time_pso)];
end
for n=2.^[2:10]
load(sprintf('saved_results/rank1_opt_n%d',n))
opt_err_n=[opt_err_n;max(opt_err)];
rtn_err_n=[rtn_err_n;max(rtn_err)];
pso_err_n=[pso_err_n;max(pso_err)];
opt_time_n=[opt_time_n;mean(time_optimal)];
pso_time_n=[pso_time_n;mean(time_pso)];
end

t=[2:11];
n=2.^[2:10];


figure(1)
semilogy(t,opt_err_t(:,1),'-o')
hold on;
semilogy(t,opt_err_t(:,2),'-*')
semilogy(t,opt_err_t(:,3),'-v')
semilogy(t,2*machepsFt+machepsFt.^2,'-k');
semilogy(t,machepsFtFt,'--k');
xlabel('$$t$$')
ylabel('$\rho_{\mathtt{OPT}}$','Interpreter','latex')
legend('$$n=16$$','$$n=128$$','$$n=1024$$','$$2v_t+v_t^2$$','$$\epsilon(F_tF_t)$$')
%exportgraphics(gcf,'figs_new/fig_rk1_opt_worst_behavior_t.pdf');


figure(2)
minline=inf;
maxline=0;
h = bar(ones(3)*nan, ones(3)*nan,'grouped');
hold on;
for nchoice = 1:3;
load(sprintf('rank1_opt_t%d',4))
%loglog(opt_err(:,nchoice),rtn_err(:,nchoice),'o','color',get(h(nchoice),'facecolor'));
loglog(rtn_err(:,nchoice),opt_err(:,nchoice),'o','color',get(h(nchoice),'facecolor'));
hold on;
minline = min(minline,min([rtn_err(:,nchoice);opt_err(:,nchoice)]));
maxline = max(maxline,max([rtn_err(:,nchoice);opt_err(:,nchoice)]));
load(sprintf('rank1_opt_t%d',8))
%loglog(opt_err(:,nchoice),rtn_err(:,nchoice),'o','color',get(h(nchoice),'facecolor'))
loglog(rtn_err(:,nchoice),opt_err(:,nchoice),'o','color',get(h(nchoice),'facecolor'))
hold on;
minline = min(minline,min([rtn_err(:,nchoice);opt_err(:,nchoice)]));
maxline = max(maxline,max([rtn_err(:,nchoice);opt_err(:,nchoice)]));
load(sprintf('rank1_opt_t%d',11))
%loglog(opt_err(:,nchoice),rtn_err(:,nchoice),'o','color',get(h(nchoice),'facecolor'))
loglog(rtn_err(:,nchoice),opt_err(:,nchoice),'o','color',get(h(nchoice),'facecolor'))
hold on;
minline = min(minline,min([rtn_err(:,nchoice);opt_err(:,nchoice)]));
maxline = max(maxline,max([rtn_err(:,nchoice);opt_err(:,nchoice)]));
end

line([minline maxline],[minline maxline]);
ylim([minline maxline]);
xlim([minline maxline]);
set(gca, 'xScale', 'log')
set(gca, 'yScale', 'log')
legend('$$n=16$$','$$n=128$$','$$n=1024$$','location','northwest');
xlabel('$\rho_{\mathtt{RTN}}$','Interpreter','latex')
ylabel('$\rho_{\mathtt{OPT}}$','Interpreter','latex')
text(5e-5,2e-4,'$$t=11$$','fontsize',18);
text(5e-4,2e-3,'$$t=8$$','fontsize',18);
text(5e-3,2e-2,'$$t=4$$','fontsize',18);
exportgraphics(gcf,'figs_new/fig_rk1_scatter_plot.pdf');
%end

figure(3)
load(sprintf('saved_results/rank1_opt_t%d',11))
boxplot(100*(1-opt_err./rtn_err),'Labels',{nn},'whisker',1000)
xlabel('$$n$$')
ylabel('Accuracy gain (\%)');
ylim([0 100]);
title('$$t=11$$')
%exportgraphics(gcf,'figs_new/fig_rk1_boxplot_opt_t11.pdf');

figure(4)
load(sprintf('saved_results/rank1_opt_n%d',128))
boxplot(100*(1-opt_err./rtn_err),'Labels',{tt},'whisker',1000)
xlabel('$$t$$')
ylabel('Accuracy gain (\%)');
ylim([0 100]);
title('$$n=128$$')
%exportgraphics(gcf,'figs_new/fig_rk1_boxplot_opt_n128.pdf');

figure(5)
semilogy(t,opt_time_t(:,1),'-o')
hold on;
semilogy(t,opt_time_t(:,2),'-*')
semilogy(t,opt_time_t(:,3),'-v')
semilogy(t,2.^t/100,'-k');
xlabel('$$t$$')
legend('$$n=16$$','$$n=128$$','$$n=1024$$','$$O(2^t)$$','location','northwest')
ylabel('time (s)')
%exportgraphics(gcf,'figs_new/fig_rk1_time_opt_t.pdf');

figure(6)
firstn=6;
nonly = n(firstn:end);
loglog(nonly,opt_time_n(firstn:end,1),'-o')
hold on;
loglog(nonly,opt_time_n(firstn:end,2),'-*')
loglog(nonly,opt_time_n(firstn:end,3),'-v')
loglog(nonly,nonly.^2/50000,'-k');
xlabel('$$n$$')
legend('$$t=4$$','$$t=8$$','$$t=11$$','$$O(n^2)$$','location','northwest')
ylabel('time (s)')
%exportgraphics(gcf,'figs_new/fig_rk1_time_opt_n.pdf');

