%%% Generates elements of Ft, FtFt, F2t

clear all; close all;
emax  = 2;

for t=2:17
  F = gen_float(t, emax);
  Ft{t} = F(F>=1 & F<=2);
  if t<=11
    F = gen_float(t, emax);
    F = gen_set_of_FP_products(F);
    FtFt{t} = F(F>=1 & F<=2);
    F = gen_float(2*t, emax);
    F2t{t} = F(F>=1 & F<=2);
  end
end
save('Ftsets.mat','Ft','FtFt','F2t');

function F = gen_float(t, emax, include_subn)
% generates a floating point number system with t bits of precision and
% (1-emax, emax) exponent range
  % include subnormals by default
  if nargin<3
    include_subn = 0;
  end
  if include_subn
    m = 1:(2^t-1);
  else
    m = (2^(t-1)):(2^t-1);
  end
  emin = 1-emax;
  F = [];
  for e=emin:emax
    F=[F m*2^(e-t)];
  end
  F=[F 2^emax];
end

function S = gen_set_of_FP_products(F)
% generate set of xy where x,y are in F
  S = [];
  for i=1:length(F)
    for j=1:length(F)
      S = [S F(i)*F(j)];
    end
  end
  % remove duplicates
  S = unique(S);
end
