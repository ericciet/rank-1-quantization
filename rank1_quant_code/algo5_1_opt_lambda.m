%%%%%%%% Implements Algorithm 5.1

function [l1opt,tbuild,tsort,topt,opterr,rtnerr,ratio]=algo5_1_opt_lambda(x,y,t,dochopy)

%% Inputs
% x, y vectors to be quantized
% t number of bits 
% dochopy=1 if both x,y needs to be quantized
% dochopy=0 if just x needs to be quantized

%% Outputs
% l1opt optimal lambda
% tbuild,tsort,topt,opterr,rtnerr,ratio



if nargin<4
  dochopy=1;
end
d=length(x);
opterr = inf;




nrmx2 = norm(x)^2;
nrmy2 = norm(y)^2;

tic;
abs(x);
x12 = abs(x)./(2.^(floor(log2(abs(x)))));

% generate breakpoints
l1 = [1];
for i=1:d
  for e=1:2
    for m=(2^(t-1)):(2^t-1)
      nextl = (m+0.5)*2^(e-t)/x12(i);
      if nextl < 2 && nextl > 1
        l1 = [l1 nextl];
      end
    end
  end
end
tbuild=toc;

% sort breakpoints
tic;
l1 = sort(l1);
tsort=toc;

% find the optimum
tic;
L1=length(l1);
l1mid = (l1(1:end-1)+l1(2:end))/2;
for i=1:L1-1
  xh = chop(l1mid(i)*x,t);
  xtxh = x'*xh;
  nrmxh2 = norm(xh)^2;
  l2 = xtxh/nrmxh2;
  if dochopy
    yh = chop(l2*y, t);
  else
    yh = l2*y;
  end
  nrmyh2 = norm(yh)^2;
  err = sqrt(nrmx2*nrmy2 + nrmxh2*nrmyh2 - 2*xtxh*(y'*yh));
  
  if err < opterr
    opterr = err;
    l1opt = l1mid(i);
  end
end
xy=x*y';
nxy=norm(xy,'fro');
opterr=opterr/nxy;
topt = toc;

% RTN strategy for comparison 
rtnerr = norm(xy-chop(x,t)*chop(y,t)', 'fro')/nxy;
ratio=rtnerr/opterr;


return



