%% Algorithm 7.3, L-factor butterfly quantization: left-to-right heuristic
% Given butterfly factors B_1,...,B_L quantizes them with t bits
% (incorportaes Algorithm 7.1)


function [q_factors,err_opt,err_rtn,ratio,total_time]=algo7_3_left2right(factors,p,t)

% Input: factors: cell of butterfly factors 
%        p: number of factors
%        t: number of bits
% Ouput: q_factors: cell of quantized factors
%        err_opt: quantization error provided by the heuristic strategy
%        err_rtn: quantization error provided by the RTN strategy
%        ratio: err_rtn/err_opt
%        total_time: execution time

start=tic;


% size of the factors
n=2^p;




start_loc=tic;

q_factors=cell(p,1);
for k=1:p
  q_factors{k} = spones(factors{k});
end

% left to right strategy
mu = ones(n,1);
for k=1:p-1
  fprintf('Factor %d out of %d\n',k,p-1)
  
  X1 = mu.*factors{k};
  
    
  b=min(2^10,n);
  for ib=1:n/b
    irange = ((ib-1)*b+1):(ib*b);
    X2 = factors{p}(irange,:)'; 
    for ii=p-1:-1:k+1
      X2 = factors{ii}'*X2;
    end
    for iloc=1:b
      i = (ib-1)*b+iloc;
      xd=X1(:,i);
      [ix,~,xs]=find(xd);
      yd = X2(:,iloc);
      [iy,~,ys]=find(yd);

      [lopt]=algo5_1_opt_lambda_butterfly(xs, ys, t, k==p-1);
      
      xsq=chop(lopt*xs, t);
      mu(i) = (xs'*xsq)/norm(xsq)^2;
      q_factors{k}(ix,i)=xsq;
      if k==p-1
        ysq=chop(mu(i)*ys, t);
        q_factors{p}(i,iy)=ysq';
      end
    end
  end

end

time_opt=toc(start_loc);

% RTN strategy for comparison
rtn_factors = cell(p);
for i=1:p
  [I,J,V]=find(factors{i});
  rtn_factors{i} = sparse(I,J,chop(V,t));
end

% probabilistic estimation of the error, avoiding to form the product of
% the factors
W = randn(n,32);
W_true = W;
W_rtn = W;
W_opt = W;
for i=p:-1:1
  W_true = factors{i}*W_true;
  W_opt = q_factors{i}*W_opt;
  W_rtn = rtn_factors{i}*W_rtn;
end
nW = norm(W_true,'fro');
err_opt(ip,it)=norm(W_true-W_opt,'fro')/nW;
err_rtn(ip,it)=norm(W_true-W_rtn,'fro')/nW;
ratio=err_rtn/err_opt;


total_time=toc(start);
end









